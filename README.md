# Cards step project

Step Project "Cards"

## Description
Step project after module Advanced JS.

A web-page where user can create, edit and view visits to doctors.
Before log in you have to register here https://ajax.test-danit.com/front-pages/cards-register.html

## Used technologies

- HTML 5
- CSS 3
- Vanilla JS
- Bootstrap 5
- AJAX


## Team

Olena Mits


## Tasks

Olena Mits:
- create HTML page
- create classes "Modal", "Cards" and all child classes
- create send requests and local storage
- create: Signin, Create visit, Edit visit, Delete visit
- Readme
- Drag and Drop