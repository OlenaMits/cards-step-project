'use strict'

import { deleteCard } from '../services/sendRequest.js'
import ModalEditCard from './ModalEditCard.js';

class Visit {
  #id;
  #doctor;
  #purpose;
  #description;
  #urgency;
  #name;
  #status;
  #card;
  #form;
  cardBody;
  formAdditionalInputs;
  constructor(id, doctor, purpose = '', description = '', urgency, name = '', status = '') {
      this.#id = id;
      this.#doctor = doctor;
      this.#purpose = purpose;
      this.#description = description;
      this.#urgency = urgency;
      this.#name = name;
      this.#status = status;
      this.#card = document.createElement('div');
      this.#form = document.createElement('div');
  }

  #prepareCardHtml() {
    return `
      <div class="card mb-4 box-shadow">
        <div class="card-body" style="position:relative">
          <h5 class="card-title" style="text-transform: capitalize">${this.#name}</h5>
          <p class="card-text" style="text-transform: capitalize">${this.#doctor}</p>
          <div class="d-flex justify-content-between align-items-center">
            <button class="btn btn-sm btn-outline-secondary" type="button" data-bs-toggle="collapse" data-bs-target="#card-collapse-${this.#id}" aria-expanded="false" aria-controls="card-collapse-${this.#id}">
              Show more
            </button>
            <button id="edit_btn-${this.#id}" type="button" class="btn btn-sm btn-outline-secondary">Edit</button>
          </div>
          <div class="collapse mt-2" id="card-collapse-${this.#id}">
            <div class="card card-body">
              <p class="card-text">Purpose: ${this.#purpose}</p>
              <p class="card-text">Description: ${this.#description}</p>
              <p class="card-text">Urgency: ${this.#urgency}</p>
              <p class="card-text">Status: ${this.#status}</p>
              <div>${this.cardBody}</div>
            </div>
          </div>
          <button id="delete-card-${this.#id}" type="button" class="btn-close d-flex" aria-label="Delete" style="position:absolute; top: 18px; right:15px"></button>
        </div>
      </div>
    `;
  }

  renderCard(parent) {
    this.#card.innerHTML = '';
    this.#card.id = `${this.#id}`;
    this.#card.classList.add('col-md-4', 'draggable', 'py-2');
    this.#card.setAttribute('draggable', true);
    this.#card.insertAdjacentHTML('beforeend', this.#prepareCardHtml());
    
    parent.append(this.#card);
  }

  getId() {
    return this.#id;
  }

  getDoctor() {
    return this.#doctor.toLowerCase();
  }

  getUrgency() {
    return this.#urgency.toLowerCase();
  }

  getStatus() {
    return this.#status.toLowerCase();
  }

  getName() {
    return this.#name.toLowerCase();
  }
  
  #prepareFormHtml() {
    return `
      <div class="col-12">
        <div class="mb-3 ">
            <select id="select_urgency" class="form-select" required aria-label="Urgency" name="urgency">
                <option ${!this.#urgency ? 'selected' : ''} value="" selected disabled>Urgency</option>
                <option ${this.#urgency === 'high' ? 'selected' : ''} value="high">High</option>
                <option ${this.#urgency === 'normal' ? 'selected' : ''} value="normal">Normal</option>
                <option ${this.#urgency === 'low' ? 'selected' : ''} value="low">Low</option>
            </select>
            <div class="invalid-feedback">Choose urgency</div>
        </div>
      </div>
      <div class="col-12">
          <div class="mb-3 form-floating">
              <input type="text" class="form-control" id="userName" name="name" value="${this.#name}" placeholder="Name and Surname" required>
              <label for="userName" class="form-label">Name and Surname</label>
              <div class="invalid-feedback">
                  Please enter your name!
              </div>
          </div>
      </div>
      <div class="col-12">
          <div class="mb-3 form-floating">
              <input type="text" class="form-control" id="visitsPurpose" name="purpose" value="${this.#purpose}" placeholder="Purpose of the visit" required>
              <label for="visitsPurpose" class="form-label">Purpose of the visit</label>
              <div class="invalid-feedback">
                  Can't be empty!
              </div>
          </div>
      </div>
      <div class="col-12">
          <div class="mb-3 form-floating">
              <textarea class="form-control" id="shortDescription" required name="description" placeholder="Short description of the visit" style="height: 104px">${this.#description}</textarea>
              <label for="shortDescription" class="form-label">Short description of the visit</label>
              <div class="invalid-feedback">
                  Can't be empty!
              </div>
          </div>
      </div>
      <div class="mb-3 ">
          <select id="select_status" class="form-select" required aria-label="Status" name="status">
              <option ${!this.#status ? 'selected' : ''} value="" selected disabled>Status</option>
              <option ${this.#status === 'open' ? 'selected' : ''} value="open">Open</option>
              <option ${this.#status === 'done' ? 'selected' : ''} value="done">Done</option>
          </select>
          <div class="invalid-feedback">Choose status</div>
      </div>
      ${this.formAdditionalInputs}
    `;
  }

  renderForm(parent) {
    this.#form.innerHTML = '';
    this.#form.id = `${this.#id}`;
    this.#form.classList.add('col-12');
    this.#form.insertAdjacentHTML('beforeend', this.#prepareFormHtml());

    parent.append(this.#form);
  }

  update(data) {
    this.#id = data.id;
    this.#doctor = data.doctor;
    this.#purpose = data.purpose;
    this.#description = data.description;
    this.#urgency = data.urgency;
    this.#name = data.name;
    this.#status = data.status;
  }

  updateInterface() {
    this.#card.innerHTML = '';
    this.#card.insertAdjacentHTML('beforeend', this.#prepareCardHtml());

    this.#form.innerHTML = '';
    this.#form.insertAdjacentHTML('beforeend', this.#prepareFormHtml());
  }

  addDeleteListener(cb) {
    this.#card.addEventListener('click', async (e) => {
      if(e.target.id === `delete-card-${this.#id}`) {
        await deleteCard(this.#id)
          .then(() => {
            this.#card.remove();
          })
          .then(() => {
            cb(this);
          })
      }
    })
  }

  addEditListener() {
    this.#card.addEventListener('click', (e) => {
      if(e.target.id === `edit_btn-${this.#id}`) {
        const modalEditCard = new ModalEditCard('Edit visit', 'Save', 'form-edit', this);
        modalEditCard.render();
        modalEditCard.addListeners();
      }
    })
  }

  addDragnDrop() {
    const dragging = this.#card;
    const wrapper = document.getElementById('cards_wrapper');

    this.#card.addEventListener('dragstart', (e) => {
      dragging.classList.add('dragging');
      let box = dragging.getBoundingClientRect();
      let offsetX = e.clientX - box.left;
      let offsetY = e.clientY - box.top;
      console.log(box);
      console.log(offsetX, offsetY);

      e.dataTransfer.setData('text/plain', e.target.id);
      e.dataTransfer.effectAllowed = 'move';
      e.dataTransfer.setDragImage(dragging, offsetX, offsetY);
    })

    wrapper.addEventListener('dragover', (e) => {
      e.preventDefault();
      const dragEl = document.querySelector('.dragging');
      if (e.preventDefault) {
        e.preventDefault();
      }
      if (e.target.classList.contains('draggable')) {           
        const sib = this.verifySiblings(dragEl, e.target);
        if (sib) {
          e.target.parentNode.insertBefore(dragEl, e.target);
        } else {
          e.target.parentNode.insertBefore(dragEl, e.target.nextSibling);
        }
      }
    })

    this.#card.addEventListener('dragend', (e) => {
      dragging.classList.remove('dragging');
      const data = e.dataTransfer.getData('text');
    })
  }

  verifySiblings(el, sib){
    let current;
    if (sib.parentNode === el.parentNode) {
        for(current = el.previousSibling; current; current = current.previousSibling) {
          if (current === sib) return true
        }
    } else {
      return false;
    }
  }
}

export default Visit;