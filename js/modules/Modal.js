'use strict'

class Modal {
    modal;
    modalBody;
    constructor(title, btnText) {
        this.title = title;
        this.btnText = btnText;
    }

    render() {
        document.body.insertAdjacentHTML('beforeend', `
            <div class="modal fade" tabindex="-1" id="myModal">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">${this.title}</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            ${this.modalBody}
                        </div>         
                    </div>
                </div>
            </div>
        `)

        this.modal = new bootstrap.Modal('#myModal', { 
            keyboard: false
        });
        this.modal.show();
        this.modal._element.addEventListener('hidden.bs.modal', event => event.target.remove());
    }

    close() {
        this.modal.hide();
    }
}

export default Modal;