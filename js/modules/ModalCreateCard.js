'use strict'

import Modal from "./Modal.js";
import Visit from "./Visit.js";
import VisitCardiologist from "./VisitCardiologist.js";
import VisitDentist from "./VisitDentist.js";
import VisitTherapist from "./VisitTherapist.js";
import { sendCard } from "../services/sendRequest.js";

class ModalCreateCard extends Modal {
    #visit;
    constructor(title, btnText, formId, visit) {
        super(title, btnText);

        this.formId = formId;
        this.#visit = visit;
        this.#renderModalBody();
    }

    #renderModalBody() {
        const doctorsInputs = document.createElement('div');
        doctorsInputs.setAttribute('id', 'doctors_inputs');
        this.modalBody = `
            <form id="${this.formId}">
                <div class="mb-3 col-12">
                    <select id="select_doctor" name="doctor" class="form-select form-select-md" aria-label=".form-select-sm example">
                        <option selected value="" disabled>Choose doctor</option>
                        <option value="cardiologist">Cardiologist</option>
                        <option value="dentist">Dentist</option>
                        <option value="therapist">Therapist</option>
                    </select>
                </div>
                ${doctorsInputs.outerHTML}
                <div class="modal-footer">
                    <button type="submit" form="${this.formId}" class="w-100 mb-2 btn btn-lg rounded-3 btn-primary">${this.btnText}</button>
                </div>
            </form>
        `;
    }

    addSelectListener() {
        const form = document.querySelector(`#${this.formId}`);
        const select = form.querySelector('#select_doctor');
        const doctorsInputs = form.querySelector('#doctors_inputs');

        select.addEventListener('change', (e) => {
            e.preventDefault();
            doctorsInputs.innerHTML = '';
            
            switch (e.target.value) {
                case 'cardiologist':
                    this.#visit = new VisitCardiologist(0);

                    break;
                case 'dentist':
                    this.#visit = new VisitDentist(0);

                    break;
                case 'therapist':
                    this.#visit = new VisitTherapist(0);

                    break;
                default:
                    this.#visit = new Visit(0);

                    break;
            }

            this.#visit.renderForm(doctorsInputs);
        })
    }

    addSubmitListener(cb) {
        const form = document.querySelector(`#${this.formId}`);
        form.addEventListener('submit', async (e) => {
            e.preventDefault();

            const formData = new FormData(form);
            
            await sendCard(Object.fromEntries(formData))
                .then(data => {
                    const cardsWrapper = document.querySelector('#cards_wrapper');
                    this.#visit.update(data);
                    this.#visit.renderCard(cardsWrapper);
                    this.#visit.updateInterface();
                })
                .then(() => {
                    cb(this.#visit);
                })
                .finally(() => super.close())
        })
    }
}

export default ModalCreateCard;