'use strict'

import Modal from "./Modal.js";
import { editCard } from "../services/sendRequest.js";

class ModalEditCard extends Modal {
    #visit;
    constructor(title, btnText, formId, visit) {
        super(title, btnText);
        
        this.#visit = visit;
        this.formId = formId;
        this.#renderModalBody();
    }

    #renderModalBody() {
        const doctorsInputs = document.createElement('div');
        doctorsInputs.setAttribute('id', 'doctors_inputs');
        this.#visit.renderForm(doctorsInputs);

        const selectedValue = this.#visit && this.#visit.getDoctor();

        this.modalBody = `
            <form id="${this.formId}">
                <div class="mb-3 col-12">
                    <select id="select_doctor" disabled class="form-select form-select-md" aria-label=".form-select-sm example">
                        <option ${!selectedValue ? 'selected' : ''} disabled hidden>Choose doctor</option>
                        <option ${selectedValue === 'cardiologist' ? 'selected' : ''} value="cardiologist">Cardiologist</option>
                        <option  ${selectedValue === 'dentist' ? 'selected' : ''} value="dentist">Dentist</option>
                        <option  ${selectedValue === 'therapist' ? 'selected' : ''} value="therapist">Therapist</option>
                    </select>
                </div>
                ${doctorsInputs.outerHTML}
                <div class="modal-footer">
                    <button type="submit" form="${this.formId}" class="w-100 mb-2 btn btn-lg rounded-3 btn-primary">${this.btnText}</button>
                </div>
            </form>
        `;
    }

    addListeners() {
        const form = document.querySelector(`#${this.formId}`);

        form.addEventListener('submit', async (e) => {
            e.preventDefault();

            const formData = new FormData(form);
            const data = {
                ...Object.fromEntries(formData), 
                doctor: this.#visit.getDoctor()
            }

            await editCard(this.#visit.getId(), data)
                .then(data => {
                    this.#visit.update(data);
                    this.#visit.updateInterface();
                })
                .finally(() => super.close())
        })
    }
}

export default ModalEditCard;