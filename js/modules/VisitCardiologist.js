'use strict'

import Visit from './Visit.js';

class VisitCardiologist extends Visit {
    #systolicPressure = 0;
    #diastolicPressure = 0;
    #bmIndex = 0;
    #illness = '';
    #age = 0;
    constructor(id, doctor, purpose, description, urgency, name, status, systolicPressure, diastolicPressure, bmIndex, illness, age) {
        super(id, doctor, purpose, description, urgency, name, status);
        this.#systolicPressure = systolicPressure;
        this.#diastolicPressure = diastolicPressure;
        this.#bmIndex = bmIndex;
        this.#illness = illness;
        this.#age = age;
        
        this.#renderCardBody();
        this.#renderFormAdditionalInputs();
    }

    #renderCardBody() {
        this.cardBody =  `
            <p class="card-text">Body pressure: ${this.#systolicPressure}/${this.#diastolicPressure}</p>
            <p class="card-text">Body mass index: ${this.#bmIndex}</p>
            <p class="card-text">Cardiovascular diseases: ${this.#illness}</p>
            <p class="card-text">Age: ${this.#age}</p>
        `;
    }

    #renderFormAdditionalInputs() {
        this.formAdditionalInputs = `
            <div class="col-md-6 col-sm-12">
                <div class="mb-2 form-floating">
                    <input type="number" required class="form-control" name="age" value="${this.#age}" id="age" placeholder="Age" min="1" max="130">
                    <label for="age" class="form-label">Age</label>
                    <div class="invalid-feedback">
                        Enter your age
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-12">
                <div class="mb-2 form-floating">
                    <input type="number" required class="form-control" name="bmIndex" value="${this.#bmIndex}" id="bmIndex" placeholder="Body mass index" min="10" max="55">
                    <label for="bmi" class="form-label">Body mass index</label>
                    <div class="invalid-feedback">
                        Enter a number
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="input-group mb-2">
                    <span class="input-group-text">Body pressure</span>
                    <input type="number" required id="Systolic" placeholder="Systolic" name="systolicPressure" value="${this.#systolicPressure}" class="form-control" min="50" max="250">
                    <input type="number" required id="Diastolic" placeholder="Diastolic" name="diastolicPressure" value="${this.#diastolicPressure}" class="form-control" min="30" max="180">
                </div>
            </div>
            <div class="col-12">
                <div class="mb-2 form-floating">
                    <input type="text" required class="form-control" name="illness" value="${this.#illness}" id="illness" placeholder="Cardiovascular diseases">
                    <label for="diseases" class="form-label">Cardiovascular diseases</label>
                    <div class="invalid-feedback">
                        Can't be empty!
                    </div>
                </div>
            </div>
        `;
    }

    update(data) {
        this.#systolicPressure = data.systolicPressure;
        this.#diastolicPressure = data.diastolicPressure;
        this.#bmIndex = data.bmIndex;
        this.#illness = data.illness;
        this.#age = data.age;
        super.update(data);
    }

    updateInterface() {
        this.#renderCardBody();
        this.#renderFormAdditionalInputs();
        super.updateInterface();
    }
}

export default VisitCardiologist;