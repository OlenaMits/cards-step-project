'use strict';

import Modal from "./Modal.js";
import { getToken } from "../services/sendRequest.js";

class ModalSignin extends Modal {
    constructor(title, btnText, formId) {
        super(title, btnText);

        this.formId = formId;
        this.modalBody = `
            <form id="${this.formId}">
                <div class="form-floating mb-3">
                    <input type="email" class="form-control rounded-3" id="inputEmail" placeholder="name@example.com" required>
                    <label for="inputEmail">Email address</label>
                </div>
                <div class="form-floating mb-3">
                    <input type="password" class="form-control rounded-3" id="inputPassword" placeholder="Password" required autocomplete="on">
                    <label for="inputPassword">Password</label>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="btnSignin" form="${this.formId}" class="w-100 mb-2 btn btn-lg rounded-3 btn-primary">${this.btnText}</button>
                </div>
            </form>
        ` 
    }

    addListener(cb) {
        const form = document.querySelector(`#${this.formId}`);

        const btnSignin = document.getElementById('signin-btn');
        const btnsGroup = document.getElementById('header_btns');
        const search = document.getElementById('filter-form');
        const cardsWrapper = document.querySelector('.cards');

        form.addEventListener('submit', async (e) => {
            e.preventDefault();
            const email =  form.querySelector('#inputEmail').value;
            const password = form.querySelector('#inputPassword').value; 
                     
            if(email && password) {
                await getToken(email, password)
                    .then((data) => {
                        if(data) {
                            btnSignin.classList.add('visually-hidden');
                            btnsGroup.classList.remove('visually-hidden');
                            search.classList.remove('visually-hidden');
                            cardsWrapper.classList.remove('visually-hidden');
                            cb();
                        } else {
                            alert('Wrong email or password!')
                        }
                    })
                    .finally(() => super.close())
            } else {
                console.log('Check your email and password!')
            }
        })
    }
}

export default ModalSignin;