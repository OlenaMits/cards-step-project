'use strict'

import Visit from './Visit.js';

class VisitTherapist extends Visit {
    #age = 0;
    constructor(id, doctor, purpose, description, urgency, name, status, age) {
        super(id, doctor, purpose, description, urgency, name, status);
        this.#age = age;
    
        this.#renderCardBody();
        this.#renderFormAdditionalInputs();
    }

    #renderCardBody() {
        this.cardBody =  `
            <p class="card-text">Age: ${this.#age}</p>
        `;
    }

    #renderFormAdditionalInputs() {
        this.formAdditionalInputs = `
            <div class="col-12">
                <div class="mb-2 form-floating">
                    <input type="number" required class="form-control" name="age" value="${this.#age}" id="age" placeholder="Age" min="1" max="110">
                    <label for="age" class="form-label">Age</label>
                    <div class="invalid-feedback">
                        Enter your age
                    </div>
                </div>
            </div>
        `;
    }

    update(data) {
        this.#age = data.age;
        super.update(data);
    }

    updateInterface() {
        this.#renderCardBody();
        this.#renderFormAdditionalInputs();
        super.updateInterface();
    }
}

export default VisitTherapist;