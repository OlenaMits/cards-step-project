'use strict'

import { getCards } from "../services/sendRequest.js";
import Visit from "./Visit.js";
import VisitCardiologist from "./VisitCardiologist.js";
import VisitDentist from "./VisitDentist.js";
import VisitTherapist from "./VisitTherapist.js";

class VisitCards {
    #cards;
    #filters = {};
    constructor() {
        this.#cards = [];
        this.deleteCard = this.#deleteCard.bind(this);
        this.addCard = this.#addCard.bind(this);
        this.render = this.#render.bind(this);
    }

    async #render() {
        await getCards()
            .then(data => {
                const wrapper = document.querySelector('#cards_wrapper');
                const noCards = document.querySelector('.empty_wrapper');
                
                if(data.length === 0) {
                    noCards.classList.remove('visually-hidden');
                } else {
                    noCards.classList.add('visually-hidden');
                    this.#cards = data.map(({ id, doctor, purpose, description, urgency, name, status, systolicPressure, diastolicPressure, bmIndex, illness, age, dateOfLastVisit }) => {
                        const value = doctor.toLowerCase(); 

                        switch (value) {
                            case 'cardiologist':
                                return new VisitCardiologist(id, doctor, purpose, description, urgency, name, status, systolicPressure, diastolicPressure, bmIndex, illness, age);
                            case 'dentist':
                                return new VisitDentist(id, doctor, purpose, description, urgency, name, status, dateOfLastVisit);
                            case 'therapist':
                                return new VisitTherapist(id, doctor, purpose, description, urgency, name, status, age);
                            default:
                                return new Visit(id, doctor, purpose, description, urgency, name, status,);
                        }
                    });
                    this.#cards.forEach(visit => {
                        visit.renderCard(wrapper);
                        visit.addDeleteListener(this.deleteCard);
                        visit.addEditListener();
                        visit.addDragnDrop();
                    })
                }
            })
    }

    #deleteCard(visit) {
        this.#cards = this.#cards.filter(card => card !== visit);
    }

    #addCard(visit) {
        this.#cards.push(visit);
        visit.addDeleteListener(this.deleteCard);
        visit.addEditListener();
    }

    setFilters(filters) {
        this.#filters = { ...this.#filters, ...filters };
        
        const cardsFiltered = this.#cards.filter(card => {
            const urgencyCheck = 'urgency' in this.#filters && this.#filters.urgency 
            ? card.getUrgency() === this.#filters.urgency 
            : true;
            
            const statusCheck = 'status' in this.#filters && this.#filters.status 
            ? card.getStatus() === this.#filters.status
            : true;
            
            const nameCheck = card.getName().includes(this.#filters.name?.toLowerCase() || "");
            
            return urgencyCheck && statusCheck && nameCheck;
        })
        
        const wrapper = document.querySelector('#cards_wrapper');
        const noCards = document.querySelector('.empty_wrapper');
        wrapper.innerHTML = '';

        if(cardsFiltered.length === 0) {
            noCards.classList.remove('visually-hidden');
        } else {
            noCards.classList.add('visually-hidden');

            cardsFiltered.forEach(visit => {
                visit.renderCard(wrapper);
            })
        }
    }
}

export default VisitCards;