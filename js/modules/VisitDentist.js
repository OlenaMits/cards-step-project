'use strict'

import Visit from './Visit.js';

class VisitDentist extends Visit {
    #dateOfLastVisit = new Date();
    constructor(id, doctor, purpose, description, urgency, name, status, dateOfLastVisit) {
        super(id, doctor, purpose, description, urgency, name, status);
        this.#dateOfLastVisit = dateOfLastVisit;
    
        this.#renderCardBody();
        this.#renderFormAdditionalInputs();
    }

    #renderCardBody() {
        this.cardBody =  `
            <p class="card-text">Last visit: ${this.#dateOfLastVisit}</p>
        `;
    }

    #renderFormAdditionalInputs() {
        this.formAdditionalInputs = `
            <div class="col-12">
                <div class="mb-2 form-floating">
                    <input type="date" required class="form-control" name="dateOfLastVisit" value="${this.#dateOfLastVisit}" id="lastVisit" placeholder="Date of last visit">
                    <label for="date" class="form-label"Last visit</label>
                    <div class="invalid-feedback">
                        Choose date of your last visit
                    </div>
                </div>
            </div>
        `;
    }

    update(data) {
        this.#dateOfLastVisit = data.dateOfLastVisit;
        super.update(data);
    }

    updateInterface() {
        this.#renderCardBody();
        this.#renderFormAdditionalInputs();
        super.updateInterface();
    }
}

export default VisitDentist;