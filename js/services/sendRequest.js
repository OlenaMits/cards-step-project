'use strict'

import LocalStorage from "../services/localStorageService.js";

const API = 'https://ajax.test-danit.com/api/v2/cards';

const sendRequest = async(entity, method = 'GET', config) => {
    const ls = new LocalStorage();
    const token = ls.get('token');

    return await fetch(`${API}${entity}`, {
        method,
        mode: 'cors',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        },
        ...config
    })
        .then((response) => {
            if(response.ok && method !== 'DELETE') {
                if(entity === '/login') {
                    return response.text()
                } else {
                    return response.json()
                }
            }
        })
        .catch(alert)
}

const getToken = (email, password) => sendRequest('/login', 'POST', {
    body: JSON.stringify({ email: `${email}`, password: `${password}` })
})
    .then(data => {
        const ls = new LocalStorage();
        ls.set('token', data);

        return data;
    })

const sendCard = (cardData) => sendRequest('/', 'POST', {
    body: JSON.stringify(cardData)
})

const deleteCard = (cardId) => sendRequest(`/${cardId}`, 'DELETE')

const getCards = () => sendRequest('/', 'GET')

const getCard = (cardId) => sendRequest(`/${cardId}`, 'GET')

const editCard = (cardId, cardData) => sendRequest(`/${cardId}`, 'PUT', {
    body: JSON.stringify(cardData)
})

export {
    getToken,
    sendCard,
    deleteCard,
    getCards,
    getCard,
    editCard
};