'use strict'

import ModalSignin from './modules/ModalSignin.js';
import ModalCreateCard from './modules/ModalCreateCard.js';
import VisitCards from './modules/VisitCards.js';
import LocalStorage from './services/localStorageService.js';

window.onload = function() {
    const ls = new LocalStorage();
    const token = ls.get('token');

    const visitCards = new VisitCards();

    const btnSignin = document.getElementById('signin-btn');
    const btnsGroup = document.getElementById('header_btns');
    const btnSignout = document.getElementById('signout-btn');
    const btnCreate = document.getElementById('create-btn');
    const search = document.getElementById('filter-form');
    const cardsSection = document.querySelector('.cards');

    const inputTitle = document.getElementById('text_area');
    const inputStatus = document.getElementById('input_status');
    const inputUrgency = document.getElementById('input_urgency');
    
    if(token && token !== 'undefined') {
        btnSignin.classList.add('visually-hidden');
        btnsGroup.classList.remove('visually-hidden');
        search.classList.remove('visually-hidden');
        cardsSection.classList.remove('visually-hidden');
        
        visitCards.render();
    } else {
        btnSignin.classList.remove('visually-hidden');
        btnsGroup.classList.add('visually-hidden');
        search.classList.add('visually-hidden');
        cardsSection.classList.add('visually-hidden');
    }

    btnSignin.addEventListener('click', (e) => {
        e.preventDefault();
        
        const modalWindow = new ModalSignin('Sign in','Sign in', 'form-signin');
        modalWindow.render();
        modalWindow.addListener(visitCards.render);
    })

    btnCreate.addEventListener('click', (e) => {
        e.preventDefault();
        
        const modalCreateCard = new ModalCreateCard('Create a visit', 'Add visit', 'form-create');
        modalCreateCard.render();
        modalCreateCard.addSelectListener();
        modalCreateCard.addSubmitListener(visitCards.addCard);
    })

    btnSignout.addEventListener('click', (e) => {
        e.preventDefault();

        ls.clear();   
        location.reload();
    })

    inputTitle.addEventListener('keyup', (e) => {
        e.preventDefault();
        
        let name = e.target.value;
        visitCards.setFilters({ name })
    })
    
    inputStatus.addEventListener('change', (e) => {
        e.preventDefault();
        
        const status = e.target.value;
        visitCards.setFilters({ status });
    })
    
    inputUrgency.addEventListener('change', (e) => {
        e.preventDefault();
        
        const urgency = e.target.value;
        visitCards.setFilters({ urgency });
    })
}