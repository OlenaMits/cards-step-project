# Cards step project

Степ проект "Cards"

## Опис проекту
Степ проект по завершенню модулю Advanced JS.

Завдання створити сторінку, де людина може створювати візити до лікарів.
Перед використанням потрібно зареєструватись тут https://ajax.test-danit.com/front-pages/cards-register.html

## Використані технології

- HTML 5
- CSS 3
- Vanilla JS
- Bootstrap 5
- AJAX


## Команда

Олена Міц


## Виконані завдання

Олена Міц:
- створення HTML сторінки
- створення класів "Modal", "Cards" та дочірніх класів
- створення запитів та робота з local storage
- створення та логіка: Sign in, Create visit, Edit visit, Delete visit
- Readme
- Drag and Drop